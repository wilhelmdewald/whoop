package de.wilson.whoop.utils;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public enum RequestContentType {
    FORM_DATA, FORM_URL_ENCODED, RAW, JSON, XML;
}
