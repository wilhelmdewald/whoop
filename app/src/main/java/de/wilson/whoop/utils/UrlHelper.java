package de.wilson.whoop.utils;

import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.wilson.library.data.WDKeyValue;
import de.wilson.whoop.model.HeaderEntry;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public final class UrlHelper {

    private static final String HEADER_ENTRY_SEPARATOR = ": ";
    private static final String HEADER_ENTRY_LINE_SEPARATOR = "\n";
    private static final String URL_PARAM_SEPARATOR = "=";
    private static final String URL_PARAM_LINE_SEPARATOR = "&";

    public static String appendBaseUrlWithQueryString(@NonNull String baseUrl, @NonNull String query) {
        // Search for existing queries
        return baseUrl + "?" + query;
    }

    public static Map<String, List<String>> getQueryParamsFromURLString(@NonNull String url) throws UnsupportedEncodingException {

        Map<String, List<String>> params = new HashMap<String, List<String>>();
        String[] urlParts = url.split("\\?");
        if (urlParts.length > 1) {
            String query = urlParts[1];
            for (String param : query.split("&")) {
                String[] pair = param.split("=");
                String key = URLDecoder.decode(pair[0], "UTF-8");
                String value = "";
                if (pair.length > 1) {
                    value = URLDecoder.decode(pair[1], "UTF-8");
                }

                List<String> values = params.get(key);
                if (values == null) {
                    values = new ArrayList<String>();
                    params.put(key, values);
                }
                values.add(value);
            }
        }

        return params;
    }

    public static URL stringToUrl(@NonNull String fullUrl) throws MalformedURLException {
        return new URL(fullUrl);
    }

    public static String keyValueListToHeaderEntriesString(List<HeaderEntry> keyValues) throws UnsupportedEncodingException {
        return listToString(keyValues, false, HEADER_ENTRY_LINE_SEPARATOR, HEADER_ENTRY_SEPARATOR);
    }

    public static String keyValueListToQueryString(List<? extends WDKeyValue> keyValues, boolean encoded) throws UnsupportedEncodingException {
        return listToString(keyValues, encoded, URL_PARAM_LINE_SEPARATOR, URL_PARAM_SEPARATOR);
    }

    public static <T extends WDKeyValue> List<T> headerEntriesStringToKeyValueList(String string, Class<T> clazz) {
        return stringToKeyValueList(string, clazz, HEADER_ENTRY_LINE_SEPARATOR, HEADER_ENTRY_SEPARATOR);
    }

    public static <T extends WDKeyValue> List<T> queryStringToKeyValueList(String query, Class<T> clazz) {
        return stringToKeyValueList(query, clazz, URL_PARAM_LINE_SEPARATOR, URL_PARAM_SEPARATOR);
    }

    public static String encodeString(@NonNull String decodedString) throws UnsupportedEncodingException {
        return URLEncoder.encode(decodedString, "UTF-8");
    }

    public static String decodeString(@NonNull String encodedString) throws UnsupportedEncodingException {
        return URLDecoder.decode(encodedString, "UTF-8");
    }

    private static String listToString(List<? extends WDKeyValue> keyValues, boolean encoded, @NonNull String lineSeparator, @NonNull String separator) throws UnsupportedEncodingException {
        if (keyValues == null || keyValues.size() == 0)
            return null;

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < keyValues.size(); i++) {
            WDKeyValue keyValue = keyValues.get(i);

            stringBuilder.append(encoded ? encodeString(keyValue.getKey()) : keyValue.getKey());
            if (keyValue.getValue() != null) {
                stringBuilder.append(separator);
                stringBuilder.append(encoded ? encodeString(keyValue.getValue()) : keyValue.getValue());
            }

            if (i < (keyValues.size() - 1))
                stringBuilder.append(lineSeparator);
        }
        return stringBuilder.toString();
    }

    private static <T extends WDKeyValue> List<T> stringToKeyValueList(String string, Class<T> clazz, @NonNull String lineSeparator, @NonNull String valueSeparator) {
        if (string == null)
            return null;

        String[] keyValuePairs = string.split(lineSeparator);

        if (keyValuePairs.length > 0) {
            List<T> keyValueList = new ArrayList<>();

            for (String keyValue : keyValuePairs) {
                String[] keyValueSplit = keyValue.split(valueSeparator, 2);

                if (keyValueSplit.length <= 2) {
                    try {

                        T newKeyValue = clazz.newInstance();
                        newKeyValue.setKey(decodeString(keyValueSplit[0]));
                        newKeyValue.setValue(keyValueSplit.length == 2 ? decodeString(keyValueSplit[1]) : null);
                        keyValueList.add(newKeyValue);
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
            return keyValueList;
        }
        return null;
    }
}
