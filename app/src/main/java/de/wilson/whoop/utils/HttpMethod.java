package de.wilson.whoop.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public enum HttpMethod {
    POST, GET, PATCH, PUT, DELETE, OPTIONS, TRACE;

    public static List<String> names() {
        HttpMethod[] methods = values();
        List<String> names = new ArrayList<>(methods.length);

        for (int i = 0; i < methods.length; i++) {
            names.add(methods[i].name());
        }

        return names;
    }
}
