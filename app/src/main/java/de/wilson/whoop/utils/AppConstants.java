package de.wilson.whoop.utils;

/**
 * Created by dew on 14.03.17.
 */

public final class AppConstants {

    public static final String DB_NAME = "whoop.de";
    public static final String PREF_NAME = "whoop_pref";
    public static final int DB_VERSION = 1;

    private AppConstants() {
    }
}
