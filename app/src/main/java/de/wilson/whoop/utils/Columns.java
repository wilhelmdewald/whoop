package de.wilson.whoop.utils;

/**
 * Created by Wilhelm Dewald on 13/04/2017.
 */

public class Columns {

    public static class Projects {

        public static final String TABLE_NAME = "project";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_BASE_URL = "base_url";
        public static final String COLUMN_FOLLOW_REDIRECTS = "follow_redirects";
        public static final String COLUMN_STORE_SESSION_COOKIE = "store_session_cookie";
        public static final String COLUMN_CONNECTION_TIMEOUT = "connection_timeout";
        public static final String COLUMN_READ_TIMEOUT = "read_timeout";
        public static final String COLUMN_DELETABLE = "deletable";
    }

    public static class Requests {

        public static final String TABLE_NAME = "requests";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_PROJECT_ID = "project_id";
        public static final String COLUMN_BASE_URL = "base_url";
        public static final String COLUMN_PATH = "path";
        public static final String COLUMN_URL_PARAMS = "url_params";
        public static final String COLUMN_HTTP_METHOD = "http_method";
        public static final String COLUMN_HEADER = "header";
        public static final String COLUMN_BODY = "body";
        public static final String COLUMN_BODY_TYPE = "body_type";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_FOLLOW_REDIRECTS = "follow_redirects";
        public static final String COLUMN_STORE_SESSION_COOKIE = "store_session_cookie";
        public static final String COLUMN_CONNECTION_TIMEOUT = "connection_timeout";
        public static final String COLUMN_READ_TIMEOUT = "read_timeout";
        public static final String COLUMN_RESPONSE_STATUS = "response_status";
        public static final String COLUMN_RESPONSE_HEADER = "response_header";
        public static final String COLUMN_RESPONSE_BODY_CONTENT_TYPE = "response_body_content_type";
        public static final String COLUMN_RESPONSE_BODY = "response_message";
        public static final String COLUMN_RESPONSE_DATE_TIME = "response_date_time";

    }

}
