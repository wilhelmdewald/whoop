package de.wilson.whoop.ui.request;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import de.wilson.whoop.model.Project;
import de.wilson.whoop.model.Request;
import de.wilson.whoop.ui.base.BasePresenter;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import de.wilson.whoop.usecase.request.FetchRequestUseCase;
import de.wilson.whoop.utils.HttpMethod;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Wilhelm Dewald on 14.07.17.
 */
public class RequestPresenter extends BasePresenter<RequestView> {

    protected final FetchRequestUseCase fetchRequestUseCase;

    private final FetchDbProjectsUseCase fetchProjectsUseCase;
    private Request currentRequest;
    private ArrayAdapter<Project> projectsAdapter;
    private ArrayAdapter<HttpMethod> httpMethodsAdapter;


    public RequestPresenter(Context context, CompositeDisposable compositeDisposable, FetchRequestUseCase fetchRequestUseCase, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        super(compositeDisposable);
        this.fetchRequestUseCase = fetchRequestUseCase;
        this.fetchProjectsUseCase = fetchDbProjectsUseCase;

        this.projectsAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        this.httpMethodsAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, HttpMethod.values());
    }

    @Override
    public void attachView(RequestView view) {
        super.attachView(view);
        getCompositeDisposable().add(
                fetchProjectsUseCase.execute()
                        .onErrorReturn(new Function<Throwable, List<Project>>() {
                            @Override
                            public List<Project> apply(@NonNull Throwable throwable) throws Exception {
                                throwable.printStackTrace();
                                getView().onError(throwable.getMessage());
                                return new ArrayList<>();
                            }
                        })
                        .subscribe(new Consumer<List<Project>>() {
                            @Override
                            public void accept(@NonNull List<Project> projects) throws Exception {
                                projectsAdapter.addAll(projects);
                            }
                        }));
    }

    public Request getCurrentRequest() {
        return currentRequest;
    }

    protected void setCurrentRequest(Request currentRequest) {
        this.currentRequest = currentRequest;
    }

    public void setupProjectsSpinner(Spinner projectsSpinner) {
        projectsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        projectsSpinner.setAdapter(projectsAdapter);
    }

    public void setupHttpMethodSpinner(Spinner httpMethodSpinner) {
        httpMethodsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        httpMethodSpinner.setAdapter(httpMethodsAdapter);
    }
}
