package de.wilson.whoop.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import de.wilson.whoop.R;
import de.wilson.whoop.WhoopApplication;
import de.wilson.whoop.injector.component.ActivityComponent;
import de.wilson.whoop.injector.component.DaggerActivityComponent;
import de.wilson.whoop.injector.module.ActivityModule;
import de.wilson.whoop.injector.module.ProjectsModule;
import de.wilson.whoop.injector.module.RequestModule;

/**
 * Created by dew on 10.03.17.
 */

public class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent = DaggerActivityComponent.builder()
                .requestModule(new RequestModule())
                .projectsModule(new ProjectsModule())
                .activityModule(new ActivityModule(this))
                .applicationComponent(((WhoopApplication) getApplication()).getComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }
}
