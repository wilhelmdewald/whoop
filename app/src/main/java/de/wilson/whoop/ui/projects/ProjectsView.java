package de.wilson.whoop.ui.projects;

import de.wilson.library.mvp.WDView;

/**
 * Created by Wilhelm Dewald on 14/04/2017.
 */

public interface ProjectsView extends WDView {
    void openNewRequestFragment();
}
