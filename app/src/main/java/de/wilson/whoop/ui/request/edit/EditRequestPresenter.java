package de.wilson.whoop.ui.request.edit;

import android.content.Context;

import de.wilson.whoop.model.Request;
import de.wilson.whoop.ui.request.RequestPresenter;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import de.wilson.whoop.usecase.request.FetchRequestUseCase;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Wilhelm Dewald on 15.07.17.
 */

public class EditRequestPresenter extends RequestPresenter {

    public EditRequestPresenter(Context context, CompositeDisposable compositeDisposable, FetchRequestUseCase requestUseCase, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        super(context, compositeDisposable, requestUseCase, fetchDbProjectsUseCase);
    }

    public void loadRequest(String requestId) {
        fetchRequestUseCase
                .execute(requestId)
                .onErrorReturn(new Function<Throwable, Request>() {
                    @Override
                    public Request apply(@NonNull Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        getView().onError(throwable.getMessage());
                        return null;
                    }
                })
                .subscribe(new Consumer<Request>() {
                    @Override
                    public void accept(@NonNull Request request) throws Exception {
                        setCurrentRequest(request);
                        getView().setViewForRequest(request);
                    }
                });
    }
}
