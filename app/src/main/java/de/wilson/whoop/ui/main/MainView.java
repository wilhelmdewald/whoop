package de.wilson.whoop.ui.main;

import de.wilson.library.mvp.WDView;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */

public interface MainView extends WDView {
    void openFragmentActivity();

    void showRootFragment();
}
