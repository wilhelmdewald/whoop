package de.wilson.whoop.ui.projects;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.wilson.whoop.model.Project;
import de.wilson.whoop.ui.base.BasePresenter;
import de.wilson.whoop.usecase.project.FetchDbProjectRequestsUseCase;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Wilhelm Dewald on 14/04/2017.
 * <p>
 * Presenter for the projects fragment.
 * Handling projects recycler view adapter and
 * the action button for new Requests.
 */

public class ProjectsPresenter extends BasePresenter<ProjectsView> {

    private final FetchDbProjectsUseCase fetchDbProjectsUseCase;
    private final FetchDbProjectRequestsUseCase fetchDbProjectRequestsUseCase;
    private ProjectsAdapter projectsAdapter;
    private LinearLayoutManager layoutManager;

    public ProjectsPresenter(CompositeDisposable compositeDisposable, FetchDbProjectsUseCase fetchDbProjectsUseCase, FetchDbProjectRequestsUseCase fetchDbProjectRequestsUseCase) {
        super(compositeDisposable);
        this.fetchDbProjectsUseCase = fetchDbProjectsUseCase;
        this.fetchDbProjectRequestsUseCase = fetchDbProjectRequestsUseCase;
    }

    @Override
    public void attachView(ProjectsView view) {
        super.attachView(view);
        getCompositeDisposable().add(
                fetchDbProjectsUseCase
                        .execute()
                        .onErrorReturn(new Function<Throwable, List<Project>>() {
                            @Override
                            public List<Project> apply(@NonNull Throwable throwable) throws Exception {
                                throwable.printStackTrace();
                                getView().onError(throwable.getMessage());
                                return new ArrayList<>();
                            }
                        })
                        .subscribe(new Consumer<List<Project>>() {
                            @Override
                            public void accept(@NonNull List<Project> projects) throws Exception {
                                projectsAdapter.setProjects(projects);
                            }
                        }));
    }

    public void setupRecyclerView(RecyclerView recyclerView, Context context) {
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        projectsAdapter = new ProjectsAdapter();
        recyclerView.setAdapter(projectsAdapter);
    }
}
