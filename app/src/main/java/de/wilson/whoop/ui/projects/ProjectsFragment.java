package de.wilson.whoop.ui.projects;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.wilson.whoop.R;
import de.wilson.whoop.ui.base.BaseActivity;
import de.wilson.whoop.ui.base.BaseFragment;
import de.wilson.whoop.ui.request.create.NewRequestFragment;

/**
 * Created by Wilhelm Dewald on 14/04/2017.
 */

public class ProjectsFragment extends BaseFragment implements ProjectsView {

    @Inject
    ProjectsPresenter projectsPresenter;

    @BindView(R.id.fragment_projects_recycler_view)
    RecyclerView recyclerView;

    public static ProjectsFragment newInstance() {
        ProjectsFragment projectsFragment = new ProjectsFragment();
        return projectsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_projects, container, false);
        ButterKnife.bind(this, root);

        ((BaseActivity) getActivity()).getActivityComponent().inject(this);
        projectsPresenter.attachView(this);
        projectsPresenter.setupRecyclerView(recyclerView, getActivity());
        projectsPresenter.onCreate();

        return root;
    }

    @Override
    public String getBackStackTag() {
        return this.getClass().toString();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onError(@StringRes int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onError(String title, String message) {

    }

    @Override
    public void hideKeyboard() {

    }

    @OnClick(R.id.fragment_projects_action_button)
    public void onClickedActionButton() {
        openNewRequestFragment();
    }

    @Override
    public void openNewRequestFragment() {
        NewRequestFragment newRequestFragment = NewRequestFragment.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, newRequestFragment)
                .addToBackStack(newRequestFragment.getBackStackTag())
                .commit();
    }
}
