package de.wilson.whoop.ui.request.create;

import android.content.Context;

import de.wilson.whoop.model.Request;
import de.wilson.whoop.ui.request.RequestPresenter;
import de.wilson.whoop.ui.request.RequestView;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import de.wilson.whoop.usecase.request.FetchRequestUseCase;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Wilhelm Dewald on 15.07.17.
 */

public class NewRequestPresenter extends RequestPresenter {

    public NewRequestPresenter(Context context, CompositeDisposable compositeDisposable, FetchRequestUseCase fetchRequestUseCase, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        super(context, compositeDisposable, fetchRequestUseCase, fetchDbProjectsUseCase);
    }

    @Override
    public void attachView(RequestView view) {
        super.attachView(view);
        setCurrentRequest(new Request());
    }
}
