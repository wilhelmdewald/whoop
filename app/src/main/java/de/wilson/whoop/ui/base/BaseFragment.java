package de.wilson.whoop.ui.base;

import android.support.v4.app.Fragment;

/**
 * Created by dew on 10.03.17.
 */

public abstract class BaseFragment extends Fragment {
    private BaseActivity baseActivity;

    public abstract String getBackStackTag();
}
