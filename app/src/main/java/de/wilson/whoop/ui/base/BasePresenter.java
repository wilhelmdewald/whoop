package de.wilson.whoop.ui.base;

import de.wilson.library.mvp.WDPresenter;
import de.wilson.library.mvp.WDView;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */

public class BasePresenter<V extends WDView> implements WDPresenter<V> {

    private final CompositeDisposable compositeDisposable;
    private V view;

    public BasePresenter(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        this.compositeDisposable.dispose();
        this.view = null;
    }

    public V getView() {
        return view;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }
}
