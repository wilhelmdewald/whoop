package de.wilson.whoop.ui.request.create;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import de.wilson.whoop.ui.base.BaseActivity;
import de.wilson.whoop.ui.request.RequestFragment;

/**
 * Created by Wilhelm Dewald on 15.07.17.
 */

public class NewRequestFragment extends RequestFragment {

    @Inject
    NewRequestPresenter newRequestPresenter;

    public static NewRequestFragment newInstance() {
        NewRequestFragment newRequestFragment = new NewRequestFragment();
        return newRequestFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        ((BaseActivity) getActivity()).getActivityComponent().inject(this);
        newRequestPresenter.attachView(this);
        newRequestPresenter.setupProjectsSpinner(projectsSpinner);
        newRequestPresenter.setupHttpMethodSpinner(httpMethodSpinner);
        newRequestPresenter.onCreate();

        return root;
    }
}
