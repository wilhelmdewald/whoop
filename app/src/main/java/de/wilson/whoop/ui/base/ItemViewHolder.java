package de.wilson.whoop.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Wilhelm Dewald on 15/04/2017.
 */

public class ItemViewHolder<T> extends RecyclerView.ViewHolder {

    private T item;

    public ItemViewHolder(View itemView) {
        super(itemView);
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
