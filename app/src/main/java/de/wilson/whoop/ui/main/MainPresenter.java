package de.wilson.whoop.ui.main;

import de.wilson.whoop.injector.PerActivity;
import de.wilson.whoop.ui.base.BasePresenter;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */
@PerActivity
public class MainPresenter extends BasePresenter<MainView> {

    private FetchDbProjectsUseCase fetchDbProjectsUseCase;

    public MainPresenter(CompositeDisposable compositeDisposable,
                         FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        super(compositeDisposable);
        this.fetchDbProjectsUseCase = fetchDbProjectsUseCase;
    }

    public void onBottomNavMenuCreated() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        getView().showRootFragment();
    }
}
