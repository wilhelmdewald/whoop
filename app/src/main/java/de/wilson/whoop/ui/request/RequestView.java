package de.wilson.whoop.ui.request;

import de.wilson.library.mvp.WDView;
import de.wilson.whoop.model.Request;

/**
 * Created by Wilhelm Dewald on 14.07.17.
 */
public interface RequestView extends WDView {
    public void setViewForRequest(Request request);
}
