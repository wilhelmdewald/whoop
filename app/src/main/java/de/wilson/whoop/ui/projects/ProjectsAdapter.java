package de.wilson.whoop.ui.projects;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.wilson.wdtreelistlibrary.WDTreeListAdapter;
import de.wilson.whoop.R;
import de.wilson.whoop.model.Project;
import de.wilson.whoop.ui.base.ItemViewHolder;

/**
 * Created by Wilhelm Dewald on 15/04/2017.
 */

public class ProjectsAdapter extends WDTreeListAdapter<RecyclerView.ViewHolder, Object> {

    private static final int VIEW_TYPE_PROJECT = 0;
    private static final int VIEW_TYPE_REQUEST = 1;
    private List<Project> projects = new ArrayList<>();

    public ProjectsAdapter() {
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects.clear();
        this.projects.addAll(projects);
        notifyThatDataChanged();
    }

    @Override
    public int getItemCount(Object parent, int depth) {
        if (parent == null) {
            return projects.size();
        } else if (parent instanceof Project) {
            return ((Project) parent).getRequests().size();
        }
        return 0;
    }

    @Override
    public boolean itemIsCollapsed(Object parent, int depth) {
        return false;
    }

    @Override
    public Object getItemObject(Object parent, int pos, int depth) {
        if (parent == null) {
            return projects.get(pos);
        } else if (parent instanceof Project) {
            return ((Project) parent).getRequests().get(pos);
        }
        return null;
    }

    @Override
    public int getItemViewType(Object parent, int depth) {
        if (depth == 0) {
            return VIEW_TYPE_PROJECT;
        } else {
            return VIEW_TYPE_REQUEST;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Object parent, Object leaf, int depth) {
        if (!(holder instanceof ProjectsViewHolder)) {
        } else {
            ((ProjectsViewHolder) holder).setItem((Project) leaf);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_PROJECT) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_project, parent, false);

            return new ProjectsAdapter.ProjectsViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_REQUEST) {

        }

        return null;
    }

    /*
     * View Holder Projects
     */
    public static class ProjectsViewHolder extends ItemViewHolder<Project> {

        @BindView(R.id.view_holder_project_title)
        TextView title;

        @BindView(R.id.view_holder_project_badge)
        TextView badge;

        public ProjectsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setItem(Project item) {
            super.setItem(item);
            title.setText(item.getName());
        }
    }
}
