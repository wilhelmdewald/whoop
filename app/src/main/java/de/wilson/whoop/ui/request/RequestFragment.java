package de.wilson.whoop.ui.request;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.wilson.whoop.R;
import de.wilson.whoop.model.Request;
import de.wilson.whoop.ui.base.BaseFragment;

/**
 * Created by Wilhelm Dewald on 14.07.17.
 */

public abstract class RequestFragment extends BaseFragment implements RequestView {

    @BindView(R.id.fragment_request_projects_dropdown)
    protected Spinner projectsSpinner;

    @BindView(R.id.fragment_request_http_method_dropdown)
    protected Spinner httpMethodSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_request, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public String getBackStackTag() {
        return this.getClass().toString();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onError(@StringRes int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onError(String title, String message) {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void setViewForRequest(Request request) {

    }
}
