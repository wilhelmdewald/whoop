package de.wilson.whoop.ui.request.edit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import de.wilson.whoop.ui.base.BaseActivity;
import de.wilson.whoop.ui.request.RequestFragment;

/**
 * Created by Wilhelm Dewald on 15.07.17.
 */

public class EditRequestFragment extends RequestFragment {

    private static final String ARG_REQUEST_ID = "ARG_REQUEST_ID";

    @Inject
    EditRequestPresenter editRequestPresenter;

    public static EditRequestFragment newInstance(String requestId) {
        EditRequestFragment editRequestFragment = new EditRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_REQUEST_ID, requestId);
        editRequestFragment.setArguments(bundle);
        return editRequestFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        ((BaseActivity) getActivity()).getActivityComponent().inject(this);
        editRequestPresenter.attachView(this);
        editRequestPresenter.loadRequest(getArguments().getString(ARG_REQUEST_ID));
        editRequestPresenter.onCreate();

        return root;
    }

}
