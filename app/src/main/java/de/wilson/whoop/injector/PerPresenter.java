package de.wilson.whoop.injector;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Wilhelm Dewald on 15/03/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerPresenter {
}
