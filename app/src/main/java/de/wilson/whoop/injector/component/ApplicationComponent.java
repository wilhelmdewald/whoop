package de.wilson.whoop.injector.component;

import javax.inject.Singleton;

import dagger.Component;
import de.wilson.library.db.WDDbOpenHelper;
import de.wilson.whoop.WhoopApplication;
import de.wilson.whoop.injector.module.ApplicationModule;

/**
 * Created by dew on 10.03.17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(WhoopApplication application);

    WDDbOpenHelper dbOpenHelper();
}
