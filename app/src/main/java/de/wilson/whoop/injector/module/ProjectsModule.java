package de.wilson.whoop.injector.module;

import dagger.Module;
import dagger.Provides;
import de.wilson.library.db.WDDbOpenHelper;
import de.wilson.whoop.injector.PerActivity;
import de.wilson.whoop.service.DbRepository;
import de.wilson.whoop.usecase.project.FetchDbProjectRequestsUseCase;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;

/**
 * Created by dew on 15.03.17.
 */

@Module
public class ProjectsModule {

    @Provides
    @PerActivity
    FetchDbProjectsUseCase provideFetchDbProjectsUseCase(WDDbOpenHelper dbOpenHelper) {
        return new FetchDbProjectsUseCase(
                new DbRepository(dbOpenHelper));
    }

    @Provides
    @PerActivity
    FetchDbProjectRequestsUseCase provideFetchDbProjectRequestsUseCase(WDDbOpenHelper dbOpenHelper) {
        return new FetchDbProjectRequestsUseCase(
                new DbRepository(dbOpenHelper));
    }
}