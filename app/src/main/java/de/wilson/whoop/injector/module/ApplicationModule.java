package de.wilson.whoop.injector.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.wilson.library.db.WDDbOpenHelper;
import de.wilson.whoop.injector.ApplicationContext;
import de.wilson.whoop.injector.DatabaseInfo;
import de.wilson.whoop.injector.PreferenceInfo;
import de.wilson.whoop.utils.AppConstants;

/**
 * Created by dew on 10.03.17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @DatabaseInfo
    int provideDatabaseVersion() {
        return AppConstants.DB_VERSION;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    WDDbOpenHelper provideDbOpenHelper(@ApplicationContext Context context, @DatabaseInfo String databaseName, @DatabaseInfo int databaseVersion) {
        return new WDDbOpenHelper(context, databaseName, databaseVersion);
    }
}
