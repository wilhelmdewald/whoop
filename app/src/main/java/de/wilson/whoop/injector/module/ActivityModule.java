package de.wilson.whoop.injector.module;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import de.wilson.whoop.injector.ActivityContext;
import de.wilson.whoop.injector.PerActivity;
import de.wilson.whoop.ui.main.MainPresenter;
import de.wilson.whoop.ui.projects.ProjectsPresenter;
import de.wilson.whoop.ui.request.create.NewRequestPresenter;
import de.wilson.whoop.ui.request.edit.EditRequestPresenter;
import de.wilson.whoop.usecase.project.FetchDbProjectRequestsUseCase;
import de.wilson.whoop.usecase.project.FetchDbProjectsUseCase;
import de.wilson.whoop.usecase.request.FetchRequestUseCase;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by dew on 10.03.17.
 */

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    MainPresenter provideMainPresenter(CompositeDisposable compositeDisposable, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        return new MainPresenter(compositeDisposable, fetchDbProjectsUseCase);
    }

    @Provides
    ProjectsPresenter provideProjectsPresenter(CompositeDisposable compositeDisposable,
                                               FetchDbProjectsUseCase fetchDbProjectsUseCase,
                                               FetchDbProjectRequestsUseCase fetchDbProjectRequestsUseCase) {
        return new ProjectsPresenter(compositeDisposable, fetchDbProjectsUseCase, fetchDbProjectRequestsUseCase);
    }

    @Provides
    NewRequestPresenter provideNewRequestPresenter(@ActivityContext Context context, CompositeDisposable compositeDisposable, FetchRequestUseCase fetchRequestUseCase, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        return new NewRequestPresenter(context, compositeDisposable, fetchRequestUseCase, fetchDbProjectsUseCase);
    }

    @Provides
    EditRequestPresenter provideEditRequestPresenter(@ActivityContext Context context, CompositeDisposable compositeDisposable, FetchRequestUseCase fetchRequestUseCase, FetchDbProjectsUseCase fetchDbProjectsUseCase) {
        return new EditRequestPresenter(context, compositeDisposable, fetchRequestUseCase, fetchDbProjectsUseCase);
    }
}
