package de.wilson.whoop.injector.module;

import dagger.Module;
import dagger.Provides;
import de.wilson.library.db.WDDbOpenHelper;
import de.wilson.whoop.injector.PerActivity;
import de.wilson.whoop.service.DbRepository;
import de.wilson.whoop.usecase.request.FetchRequestUseCase;

/**
 * Created by Wilhelm Dewald on 14.07.17.
 */

@Module
public class RequestModule {

    @Provides
    @PerActivity
    FetchRequestUseCase provideFetchRequestUseCase(WDDbOpenHelper dbOpenHelper) {
        return new FetchRequestUseCase(
                new DbRepository(dbOpenHelper));
    }
}
