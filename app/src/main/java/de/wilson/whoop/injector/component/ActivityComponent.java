package de.wilson.whoop.injector.component;

import dagger.Component;
import de.wilson.whoop.injector.PerActivity;
import de.wilson.whoop.injector.module.ActivityModule;
import de.wilson.whoop.injector.module.ProjectsModule;
import de.wilson.whoop.injector.module.RequestModule;
import de.wilson.whoop.ui.main.MainActivity;
import de.wilson.whoop.ui.projects.ProjectsFragment;
import de.wilson.whoop.ui.request.create.NewRequestFragment;
import de.wilson.whoop.ui.request.edit.EditRequestFragment;

/**
 * Created by dew on 10.03.17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ProjectsModule.class, RequestModule.class})
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(ProjectsFragment projectsFragment);

    void inject(NewRequestFragment newRequestFragment);

    void inject(EditRequestFragment editRequestFragment);
}
