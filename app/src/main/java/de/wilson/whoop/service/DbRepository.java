package de.wilson.whoop.service;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import de.wilson.library.db.WDDbHelper;
import de.wilson.library.db.WDDbOpenHelper;
import de.wilson.library.db.utils.WDDbQueryStatement;
import de.wilson.whoop.model.Project;
import de.wilson.whoop.model.Request;
import de.wilson.whoop.utils.Columns;
import io.reactivex.Single;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */

public class DbRepository implements Repository {

    private WDDbOpenHelper dbOpenHelper;
    private SQLiteDatabase readableDatabase;
    private SQLiteDatabase writableDatabase;

    public DbRepository(WDDbOpenHelper dbOpenHelper) {
        this.dbOpenHelper = dbOpenHelper;
    }

    @Override
    public Single<List<Project>> getDbProjects() {
        return new WDDbHelper<Project>(dbOpenHelper)
                .findAll(readableDatabase, WDDbQueryStatement.NEW(Columns.Projects.TABLE_NAME)
                        .select(null)
                        .query(), Project.class);
    }

    @Override
    public Single<Request> getDbRequest(String requestId) {
        return new WDDbHelper<Request>(dbOpenHelper)
                .findOne(readableDatabase, WDDbQueryStatement.NEW(Columns.Requests.TABLE_NAME)
                        .select(null)
                        .equals(Columns.Requests.COLUMN_ID, requestId).query(), Request.class);
    }

    @Override
    public Single<List<Request>> getDbRequestsForProject(String projectId) {
        return new WDDbHelper<Request>(dbOpenHelper)
                .findAll(readableDatabase, WDDbQueryStatement.NEW(Columns.Projects.TABLE_NAME)
                        .select(null)
                        .equals(Columns.Requests.COLUMN_PROJECT_ID, projectId)
                        .query(), Request.class);
    }

    public DbRepository openReadableDatabase() {
        if (readableDatabase == null || !readableDatabase.isOpen()) {
            readableDatabase = dbOpenHelper.getReadableDatabase();
        }
        return this;
    }

    public DbRepository closeReadableDatabase() {
        if (readableDatabase != null && readableDatabase.isOpen()) {
            readableDatabase.close();
        }
        readableDatabase = null;
        return this;
    }

    public DbRepository openWritableDatabase() {
        if (writableDatabase == null || !writableDatabase.isOpen()) {
            writableDatabase = dbOpenHelper.getWritableDatabase();
        }
        return this;
    }

    public DbRepository closeWritableDatabase() {
        if (writableDatabase != null && writableDatabase.isOpen()) {
            writableDatabase.close();
        }
        writableDatabase = null;
        return this;
    }
}
