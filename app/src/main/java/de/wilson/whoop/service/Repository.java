package de.wilson.whoop.service;

import java.util.List;

import de.wilson.whoop.model.Project;
import de.wilson.whoop.model.Request;
import io.reactivex.Single;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */

public interface Repository {

    Single<List<Project>> getDbProjects();

    Single<Request> getDbRequest(String requestId);

    Single<List<Request>> getDbRequestsForProject(String projectId);

    // Define additional functions
}
