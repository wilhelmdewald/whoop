package de.wilson.whoop.usecase.project;

import java.util.List;

import de.wilson.library.bc.UseCase;
import de.wilson.whoop.model.Project;
import de.wilson.whoop.service.DbRepository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 */
public class FetchDbProjectsUseCase implements UseCase<List<Project>> {

    private final DbRepository repository;

    public FetchDbProjectsUseCase(DbRepository repository) {
        this.repository = repository;
    }

    @Override
    public Single<List<Project>> execute() {
        return repository
                .openReadableDatabase()
                .getDbProjects()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        repository.closeReadableDatabase();
                    }
                });
    }
}
