package de.wilson.whoop.usecase.project;

import java.util.List;

import de.wilson.whoop.model.Request;
import de.wilson.whoop.service.DbRepository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Wilhelm Dewald on 15.07.17.
 */

public class FetchDbProjectRequestsUseCase {

    private final DbRepository repository;

    public FetchDbProjectRequestsUseCase(DbRepository repository) {
        this.repository = repository;
    }

    public Single<List<Request>> execute(String projectId) {
        return repository
                .openReadableDatabase()
                .getDbRequestsForProject(projectId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        repository.closeReadableDatabase();
                    }
                });
    }
}
