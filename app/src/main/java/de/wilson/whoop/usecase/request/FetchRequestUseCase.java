package de.wilson.whoop.usecase.request;

import de.wilson.library.bc.UseCase;
import de.wilson.whoop.model.Request;
import de.wilson.whoop.service.DbRepository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Wilhelm Dewald on 14.07.17.
 */

public class FetchRequestUseCase implements UseCase<Request> {

    private final DbRepository repository;

    public FetchRequestUseCase(DbRepository dbRepository) {
        this.repository = dbRepository;
    }

    @Override
    public Single<Request> execute() {
        return null;
    }
    
    public Single<Request> execute(String key) {
        return repository
                .openReadableDatabase()
                .getDbRequest(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        repository.closeReadableDatabase();
                    }
                });
    }
}
