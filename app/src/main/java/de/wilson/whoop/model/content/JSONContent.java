package de.wilson.whoop.model.content;

import de.wilson.whoop.utils.RequestContentType;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public class JSONContent extends Content {

    private Object jsonObject; // json object or json array

    public JSONContent() {
        super(RequestContentType.JSON);
    }

    public JSONContent(Object jsonObject) {
        super(RequestContentType.JSON);
        this.jsonObject = jsonObject;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }

    public Object getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(Object jsonObject) {
        this.jsonObject = jsonObject;
    }
}
