package de.wilson.whoop.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import de.wilson.library.db.utils.WDModel;
import de.wilson.whoop.utils.Columns;

/**
 * Created by Wilhelm Dewald on 14/03/2017.
 * <p>
 * Projects model class.
 */

public class Project extends WDModel {

    private String name;
    private String baseUrl;
    private Integer connectionTimeout;
    private Integer readTimeout;
    private boolean followRedirects;
    private boolean storeSessionCookies;
    private boolean deletable;

    private List<Request> requests = new ArrayList<>();

    public Project() {
    }

    @Override
    public ContentValues toContentValues() throws UnsupportedEncodingException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Columns.Projects.COLUMN_NAME, getName());
        contentValues.put(Columns.Projects.COLUMN_BASE_URL, getBaseUrl());
        contentValues.put(Columns.Projects.COLUMN_FOLLOW_REDIRECTS, isFollowRedirects());
        contentValues.put(Columns.Projects.COLUMN_STORE_SESSION_COOKIE, isStoreSessionCookies());
        contentValues.put(Columns.Projects.COLUMN_CONNECTION_TIMEOUT, getConnectionTimeout());
        contentValues.put(Columns.Projects.COLUMN_READ_TIMEOUT, getReadTimeout());
        contentValues.put(Columns.Projects.COLUMN_DELETABLE, isDeletable());
        return contentValues;
    }

    @Override
    public void fromCursor(Cursor c) {
        setId(c.getLong(c.getColumnIndexOrThrow(tableIdColumn())));
        setName(c.getString(c.getColumnIndexOrThrow(Columns.Projects.COLUMN_NAME)));

        int optionalBaseUrlIndex = c.getColumnIndexOrThrow(Columns.Projects.COLUMN_BASE_URL);
        setBaseUrl(
                c.isNull(optionalBaseUrlIndex)
                        ? null
                        : c.getString(optionalBaseUrlIndex)
        );

        setFollowRedirects(c.getInt(c.getColumnIndexOrThrow(Columns.Projects.COLUMN_FOLLOW_REDIRECTS)) == 1);
        setStoreSessionCookies(c.getInt(c.getColumnIndexOrThrow(Columns.Projects.COLUMN_STORE_SESSION_COOKIE)) == 1);

        int optionalConnectionTimeout = c.getColumnIndexOrThrow(Columns.Projects.COLUMN_CONNECTION_TIMEOUT);
        setConnectionTimeout(
                c.isNull(optionalConnectionTimeout)
                        ? null
                        : c.getInt(optionalConnectionTimeout)
        );

        int optionalReadTimeout = c.getColumnIndexOrThrow(Columns.Projects.COLUMN_READ_TIMEOUT);
        setReadTimeout(
                c.isNull(optionalReadTimeout)
                        ? null
                        : c.getInt(optionalReadTimeout)
        );
        setDeletable(c.getInt(c.getColumnIndexOrThrow(Columns.Projects.COLUMN_DELETABLE)) == 1);
    }

    /*
     * Required for array adapter
     */
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String tableName() {
        return Columns.Projects.TABLE_NAME;
    }

    @Override
    public String tableIdColumn() {
        return Columns.Projects.COLUMN_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public boolean isStoreSessionCookies() {
        return storeSessionCookies;
    }

    public void setStoreSessionCookies(boolean storeSessionCookies) {
        this.storeSessionCookies = storeSessionCookies;
    }

    public boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }
}
