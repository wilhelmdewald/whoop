package de.wilson.whoop.model.content;

import de.wilson.whoop.utils.RequestContentType;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public abstract class Content {

    private RequestContentType requestContentType;

    public Content() {
    }

    public Content(RequestContentType requestContentType) {
        this.requestContentType = requestContentType;
    }

    public abstract String toString();

    public RequestContentType getRequestContentType() {
        return requestContentType;
    }

    public void setRequestContentType(RequestContentType requestContentType) {
        this.requestContentType = requestContentType;
    }
}
