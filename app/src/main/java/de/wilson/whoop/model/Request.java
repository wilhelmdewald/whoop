package de.wilson.whoop.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.wilson.library.db.utils.WDModel;
import de.wilson.whoop.utils.Columns;
import de.wilson.whoop.utils.HttpMethod;
import de.wilson.whoop.utils.RequestContentType;
import de.wilson.whoop.utils.UrlHelper;

/**
 * Created by Wilhelm Dewald on 15/04/2017.
 * <p>
 * Request class.
 */

public class Request extends WDModel {

    private long projectId;
    private String fullUrl;
    private String baseUrl;
    private String path;
    private List<UrlParam> urlParams;
    private HttpMethod requestMethod;
    private List<HeaderEntry> headerEntries;
    private String requestContent;
    private RequestContentType requestContentType;
    private Date createdAt;

    private boolean followRedirects;
    private boolean storeSessionCookies;
    private Integer connectionTimeout;
    private Integer readTimeout;

    // Response
    private Integer responseStatus;
    private List<HeaderEntry> responseHeaderEntries;
    private String responseContentType;
    private String responseContent;
    private Date responseDate;

    public Request() {
    }

    public Request(Project project) {
        this.projectId = project.getId();
        this.fullUrl = project.getBaseUrl();
        this.followRedirects = project.isFollowRedirects();
        this.storeSessionCookies = project.isStoreSessionCookies();
        this.connectionTimeout = new Integer(project.getConnectionTimeout());
        this.readTimeout = new Integer(project.getReadTimeout());
    }

    public Request(Request request) {
        this.projectId = request.getProjectId();
        this.fullUrl = new String(request.getFullUrl());
        this.baseUrl = new String(request.getBaseUrl());
        this.path = new String(request.getPath());
        this.urlParams = new ArrayList<>(request.getUrlParams());
        this.requestMethod = request.getRequestMethod();
        this.headerEntries = new ArrayList<>(request.getHeaderEntries());
        this.requestContent = new String(request.getRequestContent());
        this.requestContentType = request.getRequestContentType();
        this.createdAt = new Date();
        this.followRedirects = request.isFollowRedirects();
        this.storeSessionCookies = request.isStoreSessionCookies();
        this.connectionTimeout = new Integer(request.getConnectionTimeout());
        this.readTimeout = new Integer(request.getReadTimeout());
    }

    @Override
    public ContentValues toContentValues() throws UnsupportedEncodingException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Columns.Requests.COLUMN_PROJECT_ID, this.getProjectId());
        contentValues.put(Columns.Requests.COLUMN_BASE_URL, this.getBaseUrl());
        contentValues.put(Columns.Requests.COLUMN_PATH, this.getPath());
        contentValues.put(Columns.Requests.COLUMN_URL_PARAMS, UrlHelper.keyValueListToQueryString(this.getUrlParams(), false));
        contentValues.put(Columns.Requests.COLUMN_HTTP_METHOD, this.getRequestMethod() != null ? this.getRequestMethod().toString() : null);
        contentValues.put(Columns.Requests.COLUMN_HEADER, UrlHelper.keyValueListToHeaderEntriesString(this.getHeaderEntries()));
        contentValues.put(Columns.Requests.COLUMN_BODY, this.getRequestContent());
        contentValues.put(Columns.Requests.COLUMN_BODY_TYPE, this.getResponseContentType() != null ? this.getRequestContentType().toString() : null);
        contentValues.put(Columns.Requests.COLUMN_CREATED_AT, this.getCreatedAt() != null ? this.getCreatedAt().getTime() : null);
        contentValues.put(Columns.Requests.COLUMN_FOLLOW_REDIRECTS, this.isFollowRedirects());
        contentValues.put(Columns.Requests.COLUMN_STORE_SESSION_COOKIE, this.isStoreSessionCookies());
        contentValues.put(Columns.Requests.COLUMN_CONNECTION_TIMEOUT, this.getConnectionTimeout());
        contentValues.put(Columns.Requests.COLUMN_READ_TIMEOUT, this.getReadTimeout());
        contentValues.put(Columns.Requests.COLUMN_RESPONSE_STATUS, this.getResponseStatus());
        contentValues.put(Columns.Requests.COLUMN_RESPONSE_HEADER, UrlHelper.keyValueListToHeaderEntriesString(this.getResponseHeaderEntries()));
        contentValues.put(Columns.Requests.COLUMN_RESPONSE_BODY_CONTENT_TYPE, this.getResponseContentType());
        contentValues.put(Columns.Requests.COLUMN_RESPONSE_BODY, this.getResponseContent());
        contentValues.put(Columns.Requests.COLUMN_RESPONSE_DATE_TIME, this.getResponseDate() != null ? this.getResponseDate().getTime() : null);
        return contentValues;
    }

    @Override
    public void fromCursor(Cursor c) {
        this.setId(c.getLong(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_ID)));
        this.setProjectId(c.getLong(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_PROJECT_ID)));
        this.setBaseUrl(c.getString(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_BASE_URL)));

        int optionalPathIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_PATH);
        this.setPath(
                c.isNull(optionalPathIndex)
                        ? null
                        : c.getString(optionalPathIndex)
        );
        int optionalUrlParamsIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_URL_PARAMS);
        this.setUrlParams(
                c.isNull(optionalUrlParamsIndex)
                        ? null
                        : UrlHelper.queryStringToKeyValueList(c.getString(optionalUrlParamsIndex), UrlParam.class)
        );
        int optionalHttpMethodIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_HTTP_METHOD);
        this.setRequestMethod(
                c.isNull(optionalHttpMethodIndex)
                        ? null
                        : HttpMethod.valueOf(c.getString(optionalHttpMethodIndex))
        );
        int optionalHeadersIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_HEADER);
        this.setHeaderEntries(
                c.isNull(optionalHeadersIndex)
                        ? null
                        : UrlHelper.headerEntriesStringToKeyValueList(c.getString(optionalHeadersIndex), HeaderEntry.class)
        );
        int optionalBodyIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_BODY);
        this.setRequestContent(
                c.isNull(optionalBodyIndex)
                        ? null
                        : c.getString(optionalBodyIndex)
        );
        int optionalBodyTypeIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_BODY_TYPE);
        this.setRequestContentType(
                c.isNull(optionalBodyTypeIndex)
                        ? null
                        : RequestContentType.valueOf(c.getString(optionalBodyIndex))
        );

        this.setCreatedAt(new Date(c.getLong(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_CREATED_AT))));
        this.setFollowRedirects(c.getInt(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_FOLLOW_REDIRECTS)) == 1);
        this.setStoreSessionCookies(c.getInt(c.getColumnIndexOrThrow(Columns.Requests.COLUMN_STORE_SESSION_COOKIE)) == 1);

        int optionalConnectionTimeoutIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_CONNECTION_TIMEOUT);
        this.setConnectionTimeout(
                c.isNull(optionalConnectionTimeoutIndex)
                        ? null
                        : c.getInt(optionalConnectionTimeoutIndex)
        );
        int optionalReadTimeoutIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_READ_TIMEOUT);
        this.setReadTimeout(
                c.isNull(optionalReadTimeoutIndex)
                        ? null
                        : c.getInt(optionalReadTimeoutIndex)
        );

        int optionalResponseStatusIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_RESPONSE_STATUS);
        this.setResponseStatus(
                c.isNull(optionalResponseStatusIndex)
                        ? null
                        : c.getInt(optionalResponseStatusIndex)
        );
        int optionalResponseHeaderIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_RESPONSE_HEADER);
        this.setResponseHeaderEntries(
                c.isNull(optionalResponseHeaderIndex)
                        ? null
                        : UrlHelper.headerEntriesStringToKeyValueList(c.getString(optionalResponseHeaderIndex), HeaderEntry.class)
        );
        int optionalResponseBodyContentTypeIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_RESPONSE_BODY_CONTENT_TYPE);
        this.setResponseContent(
                c.isNull(optionalResponseBodyContentTypeIndex)
                        ? null
                        : c.getString(optionalResponseBodyContentTypeIndex)
        );
        int optionalResponseBodyIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_RESPONSE_BODY);
        this.setResponseContent(
                c.isNull(optionalResponseBodyIndex)
                        ? null
                        : c.getString(optionalResponseBodyIndex)
        );
        int optionalResponseDateTimeIndex = c.getColumnIndexOrThrow(Columns.Requests.COLUMN_RESPONSE_DATE_TIME);
        this.setResponseDate(
                c.isNull(optionalResponseBodyIndex)
                        ? null
                        : new Date(c.getLong(optionalResponseDateTimeIndex))
        );
    }

    @Override
    public String tableName() {
        return Columns.Requests.COLUMN_ID;
    }

    @Override
    public String tableIdColumn() {
        return Columns.Requests.TABLE_NAME;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getFullUrl() {
        if (getBaseUrl() != null) {
            String fullUrl = getBaseUrl() + (getPath() != null ? getPath() : "");
            if (getUrlParams() != null && getUrlParams().size() > 0) {
                try {
                    fullUrl = UrlHelper.appendBaseUrlWithQueryString(fullUrl,
                            UrlHelper.keyValueListToQueryString(getUrlParams(), true));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            return fullUrl;
        }
        return this.fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<UrlParam> getUrlParams() {
        if (urlParams == null)
            urlParams = new ArrayList<>();
        return urlParams;
    }

    public void setUrlParams(List<UrlParam> urlParams) {
        this.urlParams = urlParams;
    }

    public HttpMethod getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(HttpMethod requestMethod) {
        this.requestMethod = requestMethod;
    }

    public List<HeaderEntry> getHeaderEntries() {
        if (headerEntries == null)
            headerEntries = new ArrayList<>();
        return headerEntries;
    }

    public void setHeaderEntries(List<HeaderEntry> headerEntries) {
        this.headerEntries = headerEntries;
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public RequestContentType getRequestContentType() {
        return requestContentType;
    }

    public void setRequestContentType(RequestContentType requestContentType) {
        this.requestContentType = requestContentType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public boolean isStoreSessionCookies() {
        return storeSessionCookies;
    }

    public void setStoreSessionCookies(boolean storeSessionCookies) {
        this.storeSessionCookies = storeSessionCookies;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public List<HeaderEntry> getResponseHeaderEntries() {
        return responseHeaderEntries;
    }

    public void setResponseHeaderEntries(List<HeaderEntry> responseHeaderEntries) {
        this.responseHeaderEntries = responseHeaderEntries;
    }

    public String getResponseContentType() {
        return responseContentType;
    }

    public void setResponseContentType(String responseContentType) {
        this.responseContentType = responseContentType;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }
}
