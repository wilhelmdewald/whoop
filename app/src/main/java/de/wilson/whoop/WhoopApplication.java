package de.wilson.whoop;

import android.app.Application;

import de.wilson.whoop.injector.component.ApplicationComponent;
import de.wilson.whoop.injector.component.DaggerApplicationComponent;
import de.wilson.whoop.injector.module.ApplicationModule;

/**
 * Created by dew on 10.03.17.
 */

public class WhoopApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
