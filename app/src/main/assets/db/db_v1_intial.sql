create table project (
    _id integer primary key autoincrement,
    name text not null,
    base_url text null,
    follow_redirects integer default 1,
    store_session_cookie integer default 1,
    connection_timeout integer null,
    read_timeout integer null,
    deletable integer default 1
);

insert into project(name, deletable) values ("DEFAULT", 0);