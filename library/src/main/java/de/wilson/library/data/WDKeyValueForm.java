package de.wilson.library.data;

import android.os.Parcel;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public class WDKeyValueForm extends WDKeyValue {

    public static final Creator<WDKeyValueForm> CREATOR = new Creator<WDKeyValueForm>() {
        @Override
        public WDKeyValueForm createFromParcel(Parcel in) {
            return new WDKeyValueForm(in);
        }

        @Override
        public WDKeyValueForm[] newArray(int size) {
            return new WDKeyValueForm[size];
        }
    };

    private String mFilePath;
    private String mMimeType;
    private ValueType mValueType = ValueType.TEXT;

    public WDKeyValueForm(String key, String filePath, String mimeType) {
        super(key, "");
        mFilePath = filePath;
        mMimeType = mimeType;
    }

    public WDKeyValueForm(Parcel in) {
        super(in);
        mFilePath = in.readString();
        mMimeType = in.readString();
    }

    public WDKeyValueForm() {
    }

    public WDKeyValueForm(WDKeyValueForm keyValue) {
        super(new String(keyValue.getKey()), new String(keyValue.getValue()));
        mFilePath = new String(keyValue.getFilePath());
        mMimeType = new String(keyValue.getMimeType());
        mValueType = keyValue.getValueType();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mFilePath);
        dest.writeString(mMimeType);
    }

    public ValueType getValueType() {
        return mValueType;
    }

    public void setValueType(ValueType valueType) {
        mValueType = valueType;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public void setMimeType(String mimeType) {
        mMimeType = mimeType;
    }

    /*
     * Sets the selected value type
     */
    public enum ValueType {
        TEXT, FILE;

        public static int indexOf(ValueType valueType) {
            for (int i = 0; i < ValueType.values().length; i++) {
                if (ValueType.values()[i] == valueType) {
                    return i;
                }
            }
            return -1;
        }
    }
}
