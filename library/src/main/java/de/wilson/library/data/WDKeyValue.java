package de.wilson.library.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public class WDKeyValue implements Parcelable {

    public static final Creator<WDKeyValue> CREATOR = new Creator<WDKeyValue>() {
        @Override
        public WDKeyValue createFromParcel(Parcel in) {
            return new WDKeyValue(in);
        }

        @Override
        public WDKeyValue[] newArray(int size) {
            return new WDKeyValue[size];
        }
    };

    private String mKey;
    private String mValue;

    public WDKeyValue() {
    }

    public WDKeyValue(String key, String value) {
        mKey = key;
        mValue = value;
    }

    public WDKeyValue(Parcel in) {
        mKey = in.readString();
        mValue = in.readString();
    }

    /*
     * Parcelable stuff
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKey);
        dest.writeString(mValue);
    }

    /*
     * Getter and Setter
     */
    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }
}
