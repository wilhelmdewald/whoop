package de.wilson.library.mvp;

import android.support.annotation.StringRes;

/**
 * Created by dew on 10.03.17.
 * <p>
 * The basic view structure
 */

public interface WDView {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

    void onError(String title, String message);

    void hideKeyboard();
}
