package de.wilson.library.mvp;

/**
 * Created by dew on 10.03.17.
 */

public interface WDPresenter<V extends WDView> {
    void onCreate();

    void onStart();

    void onStop();

    void onPause();

    void attachView(V view);

    void onDetach();
}
