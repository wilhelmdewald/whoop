package de.wilson.library.http;

import android.os.Parcel;

import de.wilson.library.data.WDKeyValue;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public class WDRequestHeader extends WDKeyValue {

    public static final Creator<WDRequestHeader> CREATOR = new Creator<WDRequestHeader>() {
        @Override
        public WDRequestHeader createFromParcel(Parcel in) {
            return new WDRequestHeader(in);
        }

        @Override
        public WDRequestHeader[] newArray(int size) {
            return new WDRequestHeader[size];
        }
    };


    public WDRequestHeader(String key, String value) {
        super(key, value);
    }

    public WDRequestHeader() {
    }

    public WDRequestHeader(Parcel in) {
        super(in);
    }
}
