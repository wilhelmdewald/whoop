package de.wilson.library.http;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public interface WDRequestExceptionHandler {
    void onError(Exception e);
}
