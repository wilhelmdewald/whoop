package de.wilson.library.http;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */
public class WDResponse {

    private final int mStatus;
    private final Map<String, List<String>> mResponseHeaders;
    private final String mBody;
    private final String mResponseBodyContentType;
    private final Date mResponseDate;
    private final Boolean mIsRedirect;

    public WDResponse(int status,
                      Map<String, List<String>> responseHeaders,
                      String body,
                      String contentType,
                      Boolean isRedirect) {
        mStatus = status;
        mResponseHeaders = responseHeaders;
        mBody = body;
        mResponseBodyContentType = contentType;
        mResponseDate = new Date();
        mIsRedirect = isRedirect;
    }

    public int getStatus() {
        return mStatus;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return mResponseHeaders;
    }

    public String getBody() {
        return mBody;
    }

    public Date getResponseDate() {
        return mResponseDate;
    }

    public Boolean getRedirect() {
        return mIsRedirect;
    }

    public String getResponseBodyContentType() {
        return mResponseBodyContentType;
    }
}
