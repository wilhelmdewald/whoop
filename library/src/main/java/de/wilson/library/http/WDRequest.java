package de.wilson.library.http;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import de.wilson.library.data.WDKeyValueForm;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public class WDRequest {

    private final URL mUrl;
    private URLConnection mURLConnection;
    private WDRequestExceptionHandler mExceptionHandler;

    private WDRequest(URL url) {
        mUrl = url;
    }

    public static WDRequest NEW(URL url) {
        return new WDRequest(url);
    }

    public WDRequest onError(WDRequestExceptionHandler exceptionHandler) {
        mExceptionHandler = exceptionHandler;
        return this;
    }

    public Request setup() {

        /*
         * TODO: Check CookieHandler.
         * If the request should store session cookies.
         */
        try {
            this.mURLConnection = mUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
            if (mExceptionHandler != null) {
                mExceptionHandler.onError(e);
            }
        }
        return new Request(this.mURLConnection, mExceptionHandler);
    }

    public class BaseRequest {
        private final URLConnection mURLConnection;
        private final WDRequestExceptionHandler mExceptionHandler;

        public BaseRequest(URLConnection urlConnection, WDRequestExceptionHandler exceptionHandler) {
            this.mURLConnection = urlConnection;
            this.mExceptionHandler = exceptionHandler;
        }

        protected URLConnection getURLConnection() {
            return mURLConnection;
        }

        protected WDRequestExceptionHandler getExceptionHandler() {
            return mExceptionHandler;
        }

        public Single<WDResponse> response() {
            return Single.create(new SingleOnSubscribe<WDResponse>() {
                @Override
                public void subscribe(SingleEmitter<WDResponse> emitter) throws Exception {
                    if (getURLConnection() != null) {
                        try {
                            int status = ((HttpURLConnection) getURLConnection()).getResponseCode();
                            Map<String, List<String>> responseHeaders = getURLConnection().getHeaderFields();
                            String responseBody = getResponseBody(new BufferedInputStream(getURLConnection().getInputStream()));
                            String contentType = getURLConnection().getContentType();

                            WDResponse response = new WDResponse(status, responseHeaders, responseBody, contentType, isRedirect(status));
                            ((HttpURLConnection) getURLConnection()).disconnect();

                            emitter.onSuccess(response);
                        } catch (IOException e) {
                            e.printStackTrace();
                            emitter.onError(e);
                        }
                    } else {
                        emitter.onError(new Exception("Missing url connection"));
                    }
                }
            });
        }

        private String getResponseBody(InputStream inputStream) throws IOException {
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            return response.toString();
        }

        private boolean isRedirect(int statusCode) {
            if (statusCode != HttpURLConnection.HTTP_OK) {
                if (statusCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || statusCode == HttpURLConnection.HTTP_MOVED_PERM
                        || statusCode == HttpURLConnection.HTTP_SEE_OTHER) {
                    return true;
                }
            }
            return false;
        }
    }

    /*
     * Handling request method type
     */
    public class Request extends BaseRequest {

        public Request(URLConnection urlConnection, WDRequestExceptionHandler exceptionHandler) {
            super(urlConnection, exceptionHandler);
        }

        public RequestWithMethod setRequestMethod(WDRequestMethod httpMethod) {
            try {
                ((HttpURLConnection) mURLConnection).setRequestMethod(httpMethod.toString());
            } catch (ProtocolException e) {
                e.printStackTrace();
                if (mExceptionHandler != null) {
                    mExceptionHandler.onError(e);
                }
            }

            // Setting do output
            if (httpMethod == WDRequestMethod.PUT || httpMethod == WDRequestMethod.POST) {
                mURLConnection.setDoOutput(true);
            }

            return new RequestWithMethod(mURLConnection, mExceptionHandler);
        }

        /*
         * Handling Request settings like
         * time outs or header entries
         */
        public class RequestWithMethod extends BaseRequest {

            public RequestWithMethod(URLConnection urlConnection, WDRequestExceptionHandler exceptionHandler) {
                super(urlConnection, exceptionHandler);
            }

            public RequestWithMethod setConnectionTimeout(Integer timeOutInMillis) {
                if (timeOutInMillis != null) {
                    mURLConnection.setConnectTimeout(timeOutInMillis);
                }
                return this;
            }

            public RequestWithMethod setReadTimeout(Integer timeOutInMillis) {
                if (timeOutInMillis != null) {
                    mURLConnection.setReadTimeout(timeOutInMillis);
                }
                return this;
            }

            public RequestWithMethod setFollowRedirects(boolean followRedirects) {
                ((HttpURLConnection) mURLConnection).setInstanceFollowRedirects(followRedirects);
                return this;
            }

            public RequestWithMethod addHeader(WDRequestHeader headerEntry) {
                mURLConnection.setRequestProperty(headerEntry.getKey(), headerEntry.getValue());
                return this;
            }

            public RequestWithHeader requestContent() {
                return new RequestWithHeader(getURLConnection(), getExceptionHandler());
            }

            public RequestWithHeaderMultiPart multiPartContent() {
                return new RequestWithHeaderMultiPart(mURLConnection, mExceptionHandler);
            }

            public RequestWithMethod addHeaderEntries(List<WDRequestHeader> headerEntries) {
                for (WDRequestHeader headerEntry : headerEntries) {
                    addHeader(headerEntry);
                }
                return this;
            }

            /*
             * Handling request content here.
             * For a multipart request we should define a different
             * class structure.
             *
             * Helpings for multipart connections:
             * http://www.codejava.net/java-se/networking/upload-files-by-sending-multipart-request-programmatically
             */
            public class RequestWithHeader extends BaseRequest {
                protected PrintWriter mPrintWriter;

                public RequestWithHeader(URLConnection urlConnection, WDRequestExceptionHandler exceptionHandler) {
                    super(urlConnection, exceptionHandler);
                }

                public RequestWithHeader addRequestContent(String content) {
                    if (content != null && content.length() > 0) {
                        // Todo: Should we show a warning? because, that HttpMethod is not Post/Put?
                        // Todo: We should prevent it visual
                        if (!getURLConnection().getDoOutput()) {
                            getURLConnection().setDoOutput(true);
                        }

                        try {
                            getPrintWriter().print(content);
                        } catch (IOException e) {
                            e.printStackTrace();
                            if (mExceptionHandler != null) {
                                mExceptionHandler.onError(e);
                            }
                        }
                    }
                    return this;
                }

                protected PrintWriter getPrintWriter() throws IOException {
                    if (mPrintWriter == null) {
                        mPrintWriter = new PrintWriter(new OutputStreamWriter(mURLConnection.getOutputStream(), "UTF-8"), true);
                    }
                    return mPrintWriter;
                }

                @Override
                public Single<WDResponse> response() {
                    if (mPrintWriter != null) {
                        mPrintWriter.close();
                    }
                    return super.response();
                }
            }

            /*
             * Class that handles multi part requests content
             */
            public class RequestWithHeaderMultiPart extends RequestWithHeader {
                private static final String LINE_FEED = "\r\n";
                private static final String CHARSET = "UTF-8";
                private String mBoundary;
                private OutputStream mOutputStream;

                public RequestWithHeaderMultiPart(URLConnection urlConnection, WDRequestExceptionHandler exceptionHandler) {
                    super(urlConnection, exceptionHandler);

                    // creates a unique boundary based on time stamp
                    this.mBoundary = "===" + System.currentTimeMillis() + "===";
                    urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + mBoundary);
                }

                public RequestWithHeaderMultiPart addFormField(String name, String value, String bodyType) {
                    try {
                        getPrintWriter().append("--" + mBoundary).append(LINE_FEED);
                        getPrintWriter().append("Content-Disposition: form-data; name=\"" + name + "\"")
                                .append(LINE_FEED);
                        getPrintWriter().append("Content-Type: " + bodyType + "; charset=" + CHARSET).append(
                                LINE_FEED);
                        getPrintWriter().append(LINE_FEED);
                        getPrintWriter().append(value).append(LINE_FEED);
                        getPrintWriter().flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (mExceptionHandler != null) {
                            mExceptionHandler.onError(e);
                        }
                    }
                    return this;
                }

                public RequestWithHeaderMultiPart addFormFields(List<WDKeyValueForm> formFields) {
                    for (WDKeyValueForm formField : formFields) {
                        addFormField(formField.getKey(), formField.getValue(), "text/plain");
                    }
                    return this;
                }

                public RequestWithHeaderMultiPart addFilePart(String fieldName, File uploadFile)
                        throws IOException {
                    String fileName = uploadFile.getName();
                    getPrintWriter().append("--" + mBoundary).append(LINE_FEED);
                    getPrintWriter().append(
                            "Content-Disposition: form-data; name=\"" + fieldName
                                    + "\"; filename=\"" + fileName + "\"")
                            .append(LINE_FEED);
                    getPrintWriter().append(
                            "Content-Type: "
                                    + URLConnection.guessContentTypeFromName(fileName))
                            .append(LINE_FEED);
                    getPrintWriter().append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                    getPrintWriter().append(LINE_FEED);
                    getPrintWriter().flush();

                    FileInputStream inputStream = new FileInputStream(uploadFile);
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        getOutputStream().write(buffer, 0, bytesRead);
                    }
                    getOutputStream().flush();
                    inputStream.close();

                    getPrintWriter().append(LINE_FEED);
                    getPrintWriter().flush();

                    return this;
                }

                public RequestWithHeaderMultiPart addFileParts(List<WDKeyValueForm> fileParts) {
                    for (WDKeyValueForm file : fileParts) {
                        try {
                            addFilePart(file.getKey(), new File(file.getFilePath()));
                        } catch (IOException e) {
                            e.printStackTrace();
                            getExceptionHandler().onError(e);
                        }
                    }
                    return this;
                }

                private OutputStream getOutputStream() throws IOException {
                    if (mOutputStream == null) {
                        mOutputStream = mURLConnection.getOutputStream();
                    }
                    return mOutputStream;
                }

                @Override
                public Single<WDResponse> response() {
                    if (mPrintWriter != null) {
                        mPrintWriter.append(LINE_FEED).flush();
                        mPrintWriter.append("--" + mBoundary + "--").append(LINE_FEED);
                        mPrintWriter.close();
                    }
                    return super.response();
                }
            }
        }
    }
}
