package de.wilson.library.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public enum WDRequestMethod {
    POST, GET, PATCH, PUT, DELETE, OPTIONS, TRACE;

    public static List<String> names() {
        WDRequestMethod[] methods = values();
        List<String> names = new ArrayList<>(methods.length);

        for (int i = 0; i < methods.length; i++) {
            names.add(methods[i].name());
        }

        return names;
    }
}
