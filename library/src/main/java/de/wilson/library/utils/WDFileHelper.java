package de.wilson.library.utils;

import android.support.annotation.NonNull;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Created by Wilhelm Dewald on 01/05/2017.
 */

public class WDFileHelper {

    private static final int NUMBER_OF_INPUT_BYTES = 8192;

    public static Single<String> fileToBase64(@NonNull final String filePath) {
        return Single.create(new SingleOnSubscribe<String>() {

            @Override
            public void subscribe(SingleEmitter<String> emitter) throws Exception {
                InputStream inputStream = null; //You can get an inputStream using any IO API
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    inputStream = new FileInputStream(filePath);

                    byte[] bytes;
                    byte[] buffer = new byte[NUMBER_OF_INPUT_BYTES];
                    int bytesRead;

                    try {
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bytes = output.toByteArray();
                    String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                    emitter.onSuccess(encodedString);
                } catch (FileNotFoundException e) {
                    emitter.onError(e);
                } finally {
                    try {
                        output.close();
                        if (inputStream != null) {
                            inputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
