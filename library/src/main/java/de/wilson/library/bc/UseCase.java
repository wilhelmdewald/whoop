package de.wilson.library.bc;

import io.reactivex.Single;

/**
 * Created by dew on 10.03.17.
 * <p>
 * http://macoscope.com/blog/model-view-presenter-architecture-in-android-applications/
 */
public interface UseCase<T> {
    Single<T> execute();
}
