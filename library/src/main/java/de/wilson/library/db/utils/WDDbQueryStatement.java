package de.wilson.library.db.utils;

/**
 * Created by dew on 10.03.17.
 * <p>
 * Functional implementation of an sql query builder
 */

public class WDDbQueryStatement {

    public static Select NEW(String tableName) {
        return new Select(tableName);
    }

    /*
     * Class representing a select statement for a query
     */
    public static class Select {

        private static final String SELECT = "SELECT";
        private final String tableName;
        private StringBuilder selectStatement = new StringBuilder();

        public Select(String tableName) {
            this.tableName = tableName;
        }

        public Select select(String[] columns) {
            selectStatement.append(SELECT);
            if (columns != null && columns.length != 0) {
                for (int i = 0; i < columns.length; i++) {
                    String column = columns[i];
                    selectStatement.append(" " + column);

                    if (i < (columns.length - 1))
                        selectStatement.append(",");
                }
            } else {
                selectStatement.append(" *");
            }
            selectStatement.append(" FROM " + tableName);
            return this;
        }

        public Select where() {
            selectStatement.append(" WHERE");
            return this;
        }

        public Select equals(String key, String value) {
            selectStatement.append(" ( " + key + " = '" + value + "' )");
            return this;
        }

        public Select like(String key, String value) {
            selectStatement.append(" ( UPPER(" + key + ") LIKE '%" + value.toUpperCase() + "%' )");
            return this;
        }

        public Select and() {
            selectStatement.append(" AND");
            return this;
        }

        public Select or() {
            selectStatement.append(" OR");
            return this;
        }

        public Select orderBy(String key, boolean asc) {
            selectStatement.append(" ORDER BY " + key + sortOrder(asc));
            return this;
        }

        public Select page(int start, int count) {
            selectStatement.append(String.format(" LIMIT %d OFFSET %d", count, start));
            return this;
        }

        public String query() {
            return selectStatement.append(";").toString();
        }

        private String sortOrder(boolean asc) {
            return asc ? "ASC" : "DESC";
        }
    }
}
