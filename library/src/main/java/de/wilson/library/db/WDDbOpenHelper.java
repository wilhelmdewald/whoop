package de.wilson.library.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by dew on 13.03.17.
 */

public class WDDbOpenHelper extends SQLiteOpenHelper {

    private final int databaseVersion;
    private final Context context;

    public WDDbOpenHelper(Context context, String databaseName, int databaseVersion) {
        super(context, databaseName, null, databaseVersion, new DatabaseErrorHandler() {
            @Override
            public void onCorruption(SQLiteDatabase dbObj) {

            }
        });
        this.context = context;
        this.databaseVersion = databaseVersion;
    }

    protected String databaseMigrationsFilePrefix() {
        return "db_v";
    }

    protected String databaseMigrationsFolder() {
        return "db";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        executeMigrationsForVersion(getDatabaseVersion(), db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        executeMigrationsForVersion(getDatabaseVersion(), db);
    }

    /*
     *   Iterate through all files under assets/db/ and search for
     *   sqlite migrations files starting with the database migrations file prefix
     *   the Sql statement.
     */
    private void executeMigrationsForVersion(int version, SQLiteDatabase db) {
        String[] files;
        BufferedReader reader = null;
        try {
            files = context.getAssets().list(databaseMigrationsFolder());

            for (String file : files) {
                if (file.startsWith(databaseMigrationsFilePrefix() + version)) {

                    reader = new BufferedReader(
                            new InputStreamReader(context.getAssets().open(databaseMigrationsFolder() + "/" + file), "UTF-8"));

                    // Loop threw file content until EOF
                    StringBuilder sql = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sql.append(line);
                    }

                    // Execute sql
                    String[] statements = sql.toString().split(";");
                    for (String statement : statements) {
                        db.execSQL(statement + ";");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    public int getDatabaseVersion() {
        return databaseVersion;
    }
}
