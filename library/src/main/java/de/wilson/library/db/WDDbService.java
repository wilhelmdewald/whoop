package de.wilson.library.db;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by dew on 13.03.17.
 */

public interface WDDbService<T> {

    Single<Long> insert(SQLiteDatabase writableDatabase, T model);

    Single<T> update(SQLiteDatabase writableDatabase, T model);

    Single<List<T>> findAll(SQLiteDatabase readableDatabase, String query, Class<T> tClass);

    Single<T> findOne(SQLiteDatabase readableDatabase, String query, Class<T> tClass);
}
