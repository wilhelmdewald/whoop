package de.wilson.library.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import de.wilson.library.db.utils.WDModel;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Created by dew on 13.03.17.
 */
public class WDDbHelper<T extends WDModel> implements WDDbService<T> {

    private final WDDbOpenHelper sqlOpenHelper;

    public WDDbHelper(WDDbOpenHelper sqlOpenHelper) {
        this.sqlOpenHelper = sqlOpenHelper;
    }

    public Single<Long> insert(SQLiteDatabase writableDatabase, final T model) {
        final SQLiteDatabase database = writableDatabase;
        return Single.create(new SingleOnSubscribe<Long>() {
            @Override
            public void subscribe(SingleEmitter<Long> emitter) throws Exception {
                database.beginTransaction();
                try {
                    long id = database.insertOrThrow(model.tableName(), null, model.toContentValues());
                    database.setTransactionSuccessful();
                    emitter.onSuccess(id);
                } catch (SQLiteException e) {
                    emitter.onError(e);
                } catch (UnsupportedEncodingException e) {
                    emitter.onError(e);
                } finally {
                    database.endTransaction();
                }
            }
        });
    }

    @Override
    public Single<T> update(SQLiteDatabase writableDatabase, final T model) {
        final SQLiteDatabase database = writableDatabase;
        return Single.create(new SingleOnSubscribe<T>() {
            @Override
            public void subscribe(SingleEmitter<T> emitter) throws Exception {
                database.beginTransaction();
                try {
                    long affectedRows = database.update(model.tableName(), model.toContentValues(), model.tableIdColumn() + "=" + model.getId(), new String[0]);
                    database.setTransactionSuccessful();
                    if (affectedRows == 0) {
                        emitter.onError(new WDDbHelperException("Object hasn`t been updated"));
                    } else {
                        emitter.onSuccess(model);
                    }
                } catch (SQLiteException e) {
                    emitter.onError(e);
                } catch (UnsupportedEncodingException e) {
                    emitter.onError(e);
                } finally {
                    database.endTransaction();
                }
            }
        });
    }

    @Override
    public Single<List<T>> findAll(SQLiteDatabase readableDatabase, final String query, final Class<T> tClass) {
        final SQLiteDatabase database = readableDatabase;
        return Single.create(new SingleOnSubscribe<List<T>>() {

            @Override
            public void subscribe(SingleEmitter<List<T>> emitter) throws Exception {
                database.beginTransaction();
                try {
                    List<T> results = new ArrayList<>();
                    Cursor cursor = database.rawQuery(query, null);
                    cursor.moveToFirst();

                    while (!cursor.isAfterLast()) {
                        T object = tClass.newInstance();
                        object.fromCursor(cursor);
                        results.add(object);
                        cursor.moveToNext();
                    }
                    cursor.close();

                    emitter.onSuccess(results);
                } catch (SQLiteException e) {
                    emitter.onError(e);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    emitter.onError(new WDDbHelperException("Model class needs to define the default constructor"));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    emitter.onError(e);
                    emitter.onError(new WDDbHelperException("Model class needs to define a public default constructor"));
                } finally {
                    database.endTransaction();
                }
            }
        });
    }

    @Override
    public Single<T> findOne(SQLiteDatabase readableDatabase, final String query, final Class<T> tClass) {
        final SQLiteDatabase database = readableDatabase;
        return Single.create(new SingleOnSubscribe<T>() {

            @Override
            public void subscribe(SingleEmitter<T> emitter) throws Exception {
                database.beginTransaction();
                try {
                    Cursor cursor = database.rawQuery(query, null);
                    cursor.moveToFirst();

                    T object = null;
                    while (!cursor.isAfterLast()) {
                        object = tClass.newInstance();
                        object.fromCursor(cursor);
                    }
                    cursor.close();

                    emitter.onSuccess(object);
                } catch (SQLiteException e) {
                    emitter.onError(e);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    emitter.onError(new WDDbHelperException("Model class needs to define the default constructor"));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    emitter.onError(e);
                    emitter.onError(new WDDbHelperException("Model class needs to define a public default constructor"));
                } finally {
                    database.endTransaction();
                }
            }
        });
    }


}
