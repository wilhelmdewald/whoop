package de.wilson.library.db;

/**
 * Created by Wilhelm Dewald on 13/03/2017.
 */

class WDDbHelperException extends Throwable {

    public WDDbHelperException(String s) {
        super(s);
    }
}
