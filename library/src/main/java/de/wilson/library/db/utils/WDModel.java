package de.wilson.library.db.utils;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.UnsupportedEncodingException;

/**
 * Created by dew on 13.03.17.
 * <p>
 * Basic SQL model class. Default constructor
 * is required because we are creating new model objects
 * by using the class.newInstance() functionality.
 * <p>
 */

public abstract class WDModel {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public abstract ContentValues toContentValues() throws UnsupportedEncodingException;

    public abstract void fromCursor(Cursor c);

    public abstract String tableName();

    public abstract String tableIdColumn();
}
