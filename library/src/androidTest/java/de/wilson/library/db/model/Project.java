package de.wilson.library.db.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.UnsupportedEncodingException;

import de.wilson.library.db.utils.WDModel;

/**
 * Created by dew on 13.03.17.
 */

public class Project extends WDModel {

    private String name;

    public Project() {
    }

    @Override
    public ContentValues toContentValues() throws UnsupportedEncodingException {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", getName());
        return contentValues;
    }

    @Override
    public void fromCursor(Cursor c) {
        setName(c.getString(c.getColumnIndexOrThrow("name")));
    }

    @Override
    public String tableName() {
        return "project";
    }

    @Override
    public String tableIdColumn() {
        return "_id";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
