package de.wilson.library.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import de.wilson.library.db.model.Project;
import de.wilson.library.db.utils.WDDbQueryStatement;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static junit.framework.Assert.assertEquals;

/**
 * Created by dew on 13.03.17.
 */

@RunWith(AndroidJUnit4.class)
public class SQLTests {

    SQLTestOpenHelper openHelper;
    WDDbHelper<Project> sqLiteHandler;

    @Before
    public void setUp() {
        getTargetContext().deleteDatabase("test.db");
        openHelper = new SQLTestOpenHelper(getTargetContext(), "test.db", 1);
    }

    @After
    public void tearDown() throws Exception {
        openHelper.close();
    }

    @Test
    public void shouldExecuteFirstMigrationOnStart() {
        SQLiteDatabase database = openHelper.getReadableDatabase();
        database.beginTransaction();

        Cursor cursor = database.rawQuery("select * from project", null);
        assertEquals(cursor.getColumnCount(), 1);
    }

    @Test
    public void shouldCreateANewProject() {
        // Insert new Project
        sqLiteHandler = new WDDbHelper<>(openHelper);
        SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();
        Project project = new Project();
        project.setName("test");
        Single<Long> observable = sqLiteHandler.insert(writableDatabase, project);
        TestObserver<Long> observer = new TestObserver<>();
        observable.subscribe(observer);

        // Assertion
        observer.assertNoErrors();
        observer.assertComplete();
        observer.assertValueCount(1);
        writableDatabase.close();
    }

    @Test
    public void shouldUpdate_existingProject() {
        Project testProject = new Project();
        testProject.setName("test");

        // Insert new Project
        sqLiteHandler = new WDDbHelper<>(openHelper);
        SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();

        Single<Long> observable = sqLiteHandler.insert(writableDatabase, testProject);
        TestObserver<Long> observer = new TestObserver<>();
        observable.subscribe(observer);

        // Assertion
        observer.assertNoErrors();
        observer.assertComplete();
        observer.assertValueCount(1);
        long id = observer.completions();

        // Update Project
        testProject.setId(id);
        testProject.setName("Hallo");
        Single<Project> observable2 = sqLiteHandler.update(writableDatabase, testProject);
        TestObserver<Project> observer2 = new TestObserver<>();
        observable2.subscribe(observer2);

        observer2.assertNoErrors();
        observer2.assertValueCount(1);

        // Test query
        Cursor cursor = openHelper.getReadableDatabase().rawQuery("select * from project", null);
        assertEquals(cursor.getCount(), 1);
        cursor.moveToFirst();
        assertEquals(cursor.getString(cursor.getColumnIndex("name")), "Hallo");
        writableDatabase.close();
    }

    @Test
    public void shouldFind_allProjects() {
        // Insert new Project
        sqLiteHandler = new WDDbHelper<>(openHelper);
        SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();
        Project project = new Project();
        project.setName("test");
        Single<Long> observable = sqLiteHandler.insert(writableDatabase, project);
        TestObserver<Long> observer = new TestObserver<>();
        observable.subscribe(observer);
        writableDatabase.close();

        // Assertion
        observer.assertNoErrors();
        observer.assertComplete();
        observer.assertValueCount(1);

        // Find all
        SQLiteDatabase readableDatabase = openHelper.getReadableDatabase();
        Single<List<Project>> obserables = sqLiteHandler.findAll(readableDatabase, WDDbQueryStatement.NEW("project").select(null).query(), Project.class);
        TestObserver<List<Project>> observers = new TestObserver<>();
        obserables.subscribe(observers);
        readableDatabase.close();

        observers.assertNoErrors();
        assertEquals(observers.values().get(0).size(), 1);
    }

    @Test
    public void shouldFind_allProjectsWithCount() {
        // Insert new Projects
        sqLiteHandler = new WDDbHelper<>(openHelper);
        SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();
        Project project = new Project();
        project.setName("test");
        Single<Long> observable = sqLiteHandler.insert(writableDatabase, project);
        TestObserver<Long> observer = new TestObserver<>();
        observable.subscribe(observer);

        Project project2 = new Project();
        project2.setName("test2");
        Single<Long> observable2 = sqLiteHandler.insert(writableDatabase, project2);
        TestObserver<Long> observer2 = new TestObserver<>();
        observable2.subscribe(observer2);

        writableDatabase.close();

        // Assertion
        observer.assertNoErrors();
        observer.assertComplete();
        observer.assertValueCount(1);

        // Find all
        SQLiteDatabase readableDatabase = openHelper.getReadableDatabase();
        Single<List<Project>> obserables = sqLiteHandler.findAll(readableDatabase, WDDbQueryStatement.NEW("project")
                .select(null)
                .page(0, 1).query(), Project.class);
        TestObserver<List<Project>> observers = new TestObserver<>();
        obserables.subscribe(observers);
        readableDatabase.close();

        observers.assertNoErrors();
        assertEquals(observers.values().get(0).size(), 1);
    }
}
