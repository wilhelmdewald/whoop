package de.wilson.library.db;

import org.junit.Test;

import de.wilson.library.db.utils.WDDbQueryStatement;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by dew on 10.03.17.
 */

public class SQLUnitTests {

    @Test
    public void sqlQueryStatement_shouldCreateCorrectDefaultQueryStatement() {
        String query = WDDbQueryStatement.NEW("TEST")
                .select(null)
                .query();

        assertThat(query, is(equalTo("SELECT * FROM TEST;")));
    }

    @Test
    public void sqlQueryStatement_correctQueryStatementWithSelect() {
        String query = WDDbQueryStatement.NEW("TEST")
                .select(new String[]{"HALLO", "BLA"})
                .query();
        assertThat(query, is(equalTo("SELECT HALLO, BLA FROM TEST;")));
    }

    @Test
    public void sqlQueryStatement_correctQueryStatementWithEquals() {
        String query = WDDbQueryStatement.NEW("TEST")
                .select(new String[]{"HALLO", "BLA"})
                .where()
                .equals("HALLO", "true")
                .query();
        assertThat(query, is(equalTo("SELECT HALLO, BLA FROM TEST WHERE ( HALLO = true );")));
    }
}
