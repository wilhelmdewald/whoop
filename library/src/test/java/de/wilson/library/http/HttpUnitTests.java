package de.wilson.library.http;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Wilhelm Dewald on 26/03/2017.
 */

public class HttpUnitTests {

    private MockWebServer mockWebServer;

    @Before
    public void setUp() throws Exception {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @Test
    public void requestHandler_getRequest() throws Exception {
        // Mocking Response
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody("{}")
                .setHeader("Content-Type", "application/json"));

        HttpUrl url = mockWebServer.url("/bla");

        // Executing http connection
        Single<WDResponse> observable = WDRequest
                .NEW(url.url())
                .setup()
                .setRequestMethod(WDRequestMethod.GET)
                .addHeader(new WDRequestHeader("Accept", "application/json"))
                .response();
        TestObserver<WDResponse> testObserver = new TestObserver<>();
        observable.subscribe(testObserver);

        testObserver.assertNoErrors();
        assertEquals(testObserver.values().size(), 1);

        // Checking that, the request has the correct form
        RecordedRequest mockedRequest = mockWebServer.takeRequest();
        assertEquals("/bla", mockedRequest.getPath());
        assertEquals("GET", mockedRequest.getMethod());
        assertEquals(mockedRequest.getHeader("Accept"), "application/json");
    }

    @Test
    public void requestHandler_postJsonRequest() throws Exception {
        // Mocking Response
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody("{id: \"1234\"}")
                .setHeader("Content-Type", "application/json"));

        HttpUrl url = mockWebServer.url("/bla");

        // Executing http connection
        Single<WDResponse> observable = WDRequest
                .NEW(url.url())
                .setup()
                .setRequestMethod(WDRequestMethod.POST)
                .addHeader(new WDRequestHeader("Content-Type", "application/json"))
                .addHeader(new WDRequestHeader("Accept", "application/json"))
                .requestContent()
                .addRequestContent("{user: \"wilson\"}")
                .response();
        TestObserver<WDResponse> testObserver = new TestObserver<>();
        observable.subscribe(testObserver);

        testObserver.assertNoErrors();
        assertEquals(testObserver.values().size(), 1);
        assertEquals(testObserver.values().get(0).getBody(), "{id: \"1234\"}");

        // Checking that, the request has the correct form
        RecordedRequest mockedRequest = mockWebServer.takeRequest();
        assertEquals("/bla", mockedRequest.getPath());
        assertEquals("POST", mockedRequest.getMethod());
        assertEquals(mockedRequest.getBody().readUtf8(), "{user: \"wilson\"}");
        assertEquals(mockedRequest.getHeader("Accept"), "application/json");
    }


}
